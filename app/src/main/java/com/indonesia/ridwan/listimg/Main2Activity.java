package com.indonesia.ridwan.listimg;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

//Membuat tampilan intent

public class Main2Activity extends AppCompatActivity {

    private ImageView cm;
    private TextView tcm;
    Integer [] gambar={R.drawable.cumi,R.drawable.kakap,R.drawable.kepiting,R.drawable.terei,R.drawable.udang};
    String [] descripsi={"Cumi Bakar","Kakap Enak", "Kepiting Maknyos","Teri Mantapssss","Udang lezattsss!!!"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        cm=(ImageView)findViewById(R.id.cumi2);
        tcm=(TextView)findViewById(R.id.txcumi);

        Intent intent=getIntent();
        int data=intent.getIntExtra("id",0);

        cm.setImageResource(gambar[data]);
        tcm.setText(descripsi[data]);

    }

}
