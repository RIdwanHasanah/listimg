package com.indonesia.ridwan.listimg;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.indonesia.ridwan.listimg.adapter.MakananAdapter;

/**
 * Created by hasanah on 6/17/16.
 */
public class CustomListActivity extends AppCompatActivity {

    private ListView listview;
    String[] namamakanan={" Cumi"," Kakap"," Kepiting"," Teri"," Udang"};
    Integer [] gambar={R.drawable.cumi,R.drawable.kakap,R.drawable.kepiting,R.drawable.terei,R.drawable.udang};
    //Integer [] gambar={R.drawable.cumi,R.drawable.kakap,R.drawable.kepiting,R.drawable.terei,R.drawable.udang};

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_list);
        listview=(ListView)findViewById(R.id.listview);




        MakananAdapter adapter=new MakananAdapter(CustomListActivity.this,namamakanan,gambar);
        listview.setAdapter(adapter);

        //Intent Penghubung ke Mainactivity 2
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                Intent intent=new Intent(CustomListActivity.this,Main2Activity.class);
                intent.putExtra("id",i );
                startActivity(intent);
            }
        });
    }
}
