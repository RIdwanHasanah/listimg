package com.indonesia.ridwan.listimg.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.indonesia.ridwan.listimg.R;

/**
 * Created by hasanah on 6/17/16.
 */
public class MakananAdapter  extends ArrayAdapter<String>{

    private final Activity mContext;
    private String [] mNamamakanan;
    private Integer[] mImg;

    public MakananAdapter(Activity context,String[] namamakanan,Integer[] gambar)
    {
        super(context, R.layout.row_makanan,namamakanan);

        mContext=context;
        mNamamakanan=namamakanan;
        mImg=gambar;

    }

    @Override
    public View getView(int position, View covertView, ViewGroup parent)

    {
        LayoutInflater inflater=mContext.getLayoutInflater();

        View rowView= inflater.inflate(R.layout.row_makanan,null,true);
        ImageView img=(ImageView)rowView.findViewById(R.id.img);
        TextView judul=(TextView)rowView.findViewById(R.id.txtnamamakanan);

        img.setImageResource(mImg[position]);
        judul.setText(mNamamakanan[position]);
        return rowView;


    }
}
